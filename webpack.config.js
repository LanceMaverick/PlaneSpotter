const path = require('path');

// This plugin does stuff.
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = {
//context: path.join(__dirname, ''),
  mode: 'development',
  entry: './js/index.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
	{
    	test: /\.(png|jpg|gif|svg)$/,
    	exclude: [
      		path.resolve(__dirname, './node_modules'),
    ],
    use: {
    	loader: 'file-loader',
      	options: {
        	name: '[path][name]-[hash].[ext]',
        	outputPath: '../',
        	publicPath: '/dist',
      	},
    },
  },
    {
	  test: /\.(jpe?g|png|gif|svg)$/i,
	  loaders: [
	    'file?hash=sha512&digest=hex&name=[hash].[ext]',
	    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
	  ]
	}, 
     
      {
            test: /\.(png|jp(e*)g|svg)$/,  
            use: [{
                loader: 'url-loader',
                options: { 
                    limit: 8000, // Convert images < 8kb to base64 strings
                    name: 'images/[hash]-[name].[ext]'
                } 
            }]
	},
    
      
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'index.html')
    }),
    new CopyWebpackPlugin([
        { from: 'images', to: 'images' }
    ])
  ],
};

