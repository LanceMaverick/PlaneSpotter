import '../scss/app.scss';
const path = require('path');
var bs = require('../node_modules/bootstrap/dist/js/bootstrap.bundle');
var $ = require('jquery');


//window.$ = $;

var allPlanes = require('./aircraft.json');
var imgPath = 'images/aircraft/' // default path here
var chosenPlane; //current displayed aircraft
var filteredPlanes; //filtered json for selected nations
var quizIt = 1; //question number
var score = 0; //score tally

$( document ).ready(function() {
    $("#quiz-div").attr('display', 'none');
    $("#footer-logo").attr("src", path.join('images', 'Logo_Black_transparent_red2.png'));

//    $(".jumbotron").css('background-image', path.join('images', 'Logo_Black_transparent_red2.png'));
    console.log("Hmm... are you here to try and cheat? If so go ahead, it's just a learning excercise!");
});

//if il2 mode is checked, disable nation choices
$('#il2-mode').on("click", function(e){
    let il2 = e.currentTarget['checked']
    if (il2 === true){
        $('.natcheck').each(function(index) {
            $(this).attr("disabled", "disabled");
        });
        console.log("il2-mode")
    }
    else{
        console.log("WT mode")
        $('.natcheck').each(function(index) {
            $(this).removeAttr("disabled");
        });
    }
});

//generate flashcard quiz
$(function() {
    $('#setup-button').on("click", function(e){
        e.preventDefault();

        //check if in il2 mode. Ignore nations and use only il2 airframes
        console.log($('#il2-mode'))
        let il2 = $('#il2-mode').prop("checked")
        console.log(il2, "--")
        if(il2 === true){
            filteredPlanes = allPlanes.filter(el => el.il2 === true);
        }
        else {
            let checked = $('.natcheck').filter(function (i, el) {return el.checked === true});
            console.log(checked.length)
        
		    if(checked.length === 0){
			    console.log("here")
			    alert('choose at least one nation');
			    return
		    };

            let chosenNations = [];
            for(let i of checked) {
                chosenNations.push(i.value);
            };
            filteredPlanes = allPlanes.filter( (x) => filterX(chosenNations, x));
        }
		console.log(filteredPlanes);
        $("#quiz-div").show();
        $('#guess-button').attr("disabled", false);
        quizIt = 1;
        score = 0;
        $('#score-message').html("");
        newPlane(filteredPlanes);
    });
});

$('#next-button').click(function() {
    newPlane(filteredPlanes);
});

//grab a new plane at random from the filtered list
function newPlane(planes){
    let num = Math.floor( Math.random() * planes.length );
    chosenPlane = planes[num];
    $("#plane-img").attr("src", path.join(imgPath, chosenPlane['path']));
    $('#q-number').html("Question: "+quizIt);
    $('#answer').val('');
    $('#next-button').attr("disabled", true);
    $('#guess-button').attr("disabled", false);
};

//logic for answering the flashcard
$('#guess-button').click(function() {

    let answer = $('#answer').val().toLowerCase();
    let regExp = new RegExp(chosenPlane['regex']);
    console.log('regex:');
    if (regExp.exec(answer)){
        score +=1;
        $('#answer-message').html('<font color="green"> Correct! It\'s a '+chosenPlane['name']+'</font>');
    }
    else {
        $('#answer-message').html('<font color="red"> Incorrect! It\'s a '+chosenPlane['name']+'</font>');
    }
    $('#score-message').html("<h2>Score: "+score+"/10</h2>");
    quizIt +=1;
    if (quizIt > 10)
    {
        $('#guess-button').attr("disabled", true);
        $('#final-message').html("Your final score is: ");
    }
    else{
    $('#next-button').attr("disabled", false);
    $('#guess-button').attr("disabled", true);
    }
});
        
//filter plane choices
function filterX (nations, plane) {
    for (let i of nations) {
        if (plane.nation === i)  return true; }
     return false; 
};

//grab and kill enter press because nothing seems to change button focus or change "start" button from type submit...
$('html').bind('keypress', function(e)
{
    if(e.keyCode === 13)
    {
        return false;
     }
});
